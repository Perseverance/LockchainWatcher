pragma solidity ^0.4.15;


contract Ownable {
    address public owner;

    function Ownable() {
        owner = msg.sender;
    }

    modifier onlyOwner() {
        require(msg.sender == owner);
        _;
    }
}


contract ETHWatcher is Ownable {
    address public watchedAddress;
    bool hasEnded;
    uint finalAmount;
    
    function ETHWatcher(address _watchedAddress) {
        watchedAddress = _watchedAddress;
    }
    
    function currentAmount() public constant returns(uint) {
        if (hasEnded) {
            return finalAmount;
        }
        return watchedAddress.balance;
    }
    
    function totalSupply() public constant returns(uint) {
        return currentAmount();
    }
    
    function finalize() onlyOwner {
        hasEnded = true;
        finalAmount = watchedAddress.balance;
    }
}


contract LockChainEtherWatcher is ETHWatcher(0x5e3346444010135322268a4630d2ED5F8D09446c) {
    

}